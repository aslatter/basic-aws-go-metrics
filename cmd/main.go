package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"time"

	"example.com/demo/internal/xaws"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func main() {
	err := mainErr()
	if err != nil {
		fmt.Fprintln(os.Stderr, "error: ", err)
		os.Exit(1)
	}
}

func mainErr() error {
	var addr string
	flag.StringVar(&addr, "addr", "localhost:8080", "address to listen on")
	flag.Parse()

	ctx, close := signal.NotifyContext(context.Background(), os.Interrupt)
	defer close()

	a, err := newApp(ctx)
	if err != nil {
		return err
	}

	var handler http.ServeMux

	handler.Handle("GET /metrics", promhttp.Handler())
	handler.HandleFunc("GET /tables", a.listTables)
	handler.HandleFunc("GET /buckets", a.listBuckets)

	srv := http.Server{
		Addr:    addr,
		Handler: &handler,
	}

	return serve(ctx, &srv)
}

// serve calls ListenAndServe but with a graceful shutdown.
func serve(ctx context.Context, srv *http.Server) error {
	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		defer wg.Done()
		<-ctx.Done()
		shutdownContext, close := context.WithTimeout(context.Background(), 2*time.Second)
		defer close()
		srv.Shutdown(shutdownContext)
	}()

	fmt.Println("listening on ", srv.Addr)
	err := srv.ListenAndServe()

	wg.Wait()
	return err
}

func newApp(ctx context.Context) (*app, error) {
	cfg, err := config.LoadDefaultConfig(ctx, config.WithRegion("us-east-2"))
	if err != nil {
		return nil, err
	}

	xaws.AddMetrics(&cfg)

	a := app{
		dynamoClient: *dynamodb.NewFromConfig(cfg),
		s3Client:     *s3.NewFromConfig(cfg),
	}

	return &a, nil
}

type app struct {
	dynamoClient dynamodb.Client
	s3Client     s3.Client
}

func (a *app) listTables(rw http.ResponseWriter, req *http.Request) {
	var tables []string
	var lastTable *string

	for {
		tableResponse, err := a.dynamoClient.ListTables(req.Context(), &dynamodb.ListTablesInput{
			ExclusiveStartTableName: lastTable,
		})
		if err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintln(rw, err)
			return
		}

		tables = append(tables, tableResponse.TableNames...)

		lastTable = tableResponse.LastEvaluatedTableName
		if lastTable == nil {
			break
		}
	}

	rw.WriteHeader(http.StatusOK)
	for _, table := range tables {
		fmt.Fprintln(rw, table)
	}
}

func (a *app) listBuckets(rw http.ResponseWriter, req *http.Request) {
	output, err := a.s3Client.ListBuckets(req.Context(), nil)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintln(rw, err)
		return
	}

	rw.WriteHeader(http.StatusOK)
	for _, bucket := range output.Buckets {
		fmt.Fprintln(rw, *bucket.Name)
	}
}
