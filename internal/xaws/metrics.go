package xaws

import (
	"context"
	"strconv"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	sdkmiddleware "github.com/aws/aws-sdk-go-v2/aws/middleware"
	"github.com/aws/smithy-go/middleware"
	smithyhttp "github.com/aws/smithy-go/transport/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

func AddMetrics(cfg *aws.Config) {
	cfg.APIOptions = append(cfg.APIOptions, func(s *middleware.Stack) error {
		return s.Finalize.Add(awsMetrics(), middleware.After)
	})
}

func awsMetrics() middleware.FinalizeMiddleware {
	return middleware.FinalizeMiddlewareFunc("metrics", func(ctx context.Context, fi middleware.FinalizeInput, fh middleware.FinalizeHandler) (out middleware.FinalizeOutput, metadata middleware.Metadata, outErr error) {
		startTime := time.Now()

		defer func() {
			duration := time.Since(startTime)

			region := sdkmiddleware.GetRegion(ctx)
			svc := sdkmiddleware.GetServiceID(ctx)
			op := sdkmiddleware.GetOperationName(ctx)

			status := -1

			rawResponse := sdkmiddleware.GetRawResponse(metadata)
			if response, ok := rawResponse.(*smithyhttp.Response); ok && response != nil {
				status = response.StatusCode
			}

			durationMetric.WithLabelValues(
				region,
				svc,
				op,
				strconv.Itoa(status),
			).Observe(duration.Seconds())
		}()

		out, metadata, outErr = fh.HandleFinalize(ctx, fi)
		return out, metadata, outErr
	})
}

var durationMetric = promauto.NewHistogramVec(prometheus.HistogramOpts{
	Name:    "aws_request_duration_seconds",
	Buckets: prometheus.DefBuckets,

	// experimental
	NativeHistogramBucketFactor: 1.1,
}, []string{
	label_region,
	label_service,
	label_operation,
	label_status,
})

const (
	label_region    = "region"
	label_service   = "service"
	label_operation = "operation"
	label_status    = "status"
)
